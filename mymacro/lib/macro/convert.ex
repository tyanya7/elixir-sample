defmodule Mymacro.Convert do
  @default_p_one "あ"
  @default_p_zero "い"

  def process(input) when is_binary(input) do
    input
    |> to_charlist #文字列リストに変換
    |> Enum.map(fn x -> Integer.to_string(x, 2) end) #2進数文字列に変換
    |> Enum.map(fn x -> String.rjust(x, 8, ?0) end)
    |> Enum.map(fn x -> to_charlist x end) #各要素を文字列リストに
    |> Enum.map(&(change_binary/1)) #各文字列リストを01を元に変換する 返り値は文字列のリスト
    |> Enum.join
  end

  def change_binary(c_list, p_one \\ @default_p_one, p_zero \\ @default_p_zero) do
    Enum.map(c_list,
    fn i ->
      if i==?1 do
        p_one
      else
        p_zero
      end
    end)
  end

  def unprocess(input) when is_binary(input) do
    input
    |> to_charlist
    |> Enum.chunk(8)
    |> Enum.map(&(unchange_binary/1))
    |> to_string
  end

  def unchange_binary(c_list) do
    c_list
    |> Enum.map(fn i ->
        if i==?あ do
          "1"
        else
          "0"
        end
       end)
    |> Enum.join
    |> Integer.parse(2)
    |> Tuple.to_list
    |> List.first
  end


end

defmodule Mymacro.UnParse do

  def unparse(text, code) do
    r = Regex.named_captures(~r/{(?<func_name>(\.|\w)*)}\[(?<module_name>[^]]*)\]\((?<argument>(\,|\w|-)*)\)/, text)
    IO.inspect r
    if r do
      %{"func_name" => func_name, "module_name" => modules, "argument" => arguments} = r
      next_text = Regex.replace(~r/{(?<func_name>(\.|\w)*)}\[(?<module_name>[^]]*)\]\((?<argument>(\,|\w|-)*)\)/, text, "Done", [global: false])
      inspect next_text
      unparse(next_text, [{String.to_atom(func_name), module_unparse(modules), arguments} | code])
    else
      code
    end
  end


  def module_unparse("") do [] end
  def module_unparse(modules) do
    String.split(modules, ",")
    |> Enum.map(fn module ->
      String.split(module, ":")
      |> Enum.map(&(String.to_atom/1))
      |> List.to_tuple
    end)
    |> List.flatten
  end

end

#~r/{(?<func_name>(\w|\.)*)}\[(?<modules>\w*)\]\((?<argument>(\w|,)*)\)/
#~r/{(?<func_name>\w*)}\[(?<module_name>[^]]*)\]\((?<argument>.*)\)/
#~r/{(?<func_name>\w*)}\[(?<module_name>[^]]*)\]\((?<argument>\w*)\)/
#~r/{(?<func_name>(\.|\w)*)}\[(?<module_name>[^]]*)\]\((?<argument>(\,|\w)*)\)/
#~r/{(?<func_name>(\.|\w)*)}\[(?<module_name>[^]]*)\]\((?<argument>(\,|\w|-)*)\)/

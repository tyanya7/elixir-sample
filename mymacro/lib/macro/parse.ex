defmodule Mymacro.Parse do
  @moduledoc """
  仕様

  metadataは[key: value]の文字列で返す

  argumentは再びfunc_parse関数に渡してパターンマッチにて処理を行う
  ( )で囲むことにする

  [:func_name, [modules], [{func_info}, [do: do_block]

  {:func_name, [modules], module_atom]
  """

  defmacro can_fanc_parse(func_atom, argument) do
    quote do
      is_atom(unquote(func_atom)) and is_list(unquote(argument))
    end
  end

  @doc"""
  [{:huga, [context: Makuro], Makuro},
               [do: {{:., [], [{:__aliases__, [alias: false], [:IO]}, :puts]}, [], ["hogeeeeeeeeeeeeeeeee"]}]]
         のタイプのパース
  """
  def func_parse([do: do_block], state) do
    next_string = "do-block" <> func_parse(do_block, "")
    state <> next_string
  end

  @doc"""
  [{:__aliases__, [alias: false], [:IO]}, :puts]
  のタイプのパース
  """
  def func_parse([func_info, func_name], state) when is_atom(func_name) do
    state <> func_parse(func_info, "") <> to_string(func_name)
  end

  @doc"""
  {{:., metadata, argument}, metadata, argument} の形をパースする
  """
  def func_parse({{func_atom, metadata, argument}, metadata, argument }, state) do
    func_parse({func_atom, metadata, argument}, state <[{:__aliases__, [alias: false], [:IO]}, :puts]>
               metadata_parse(metadata) <> "(" <> func_parse(argument, state) <> ")")
  end

  @doc"""
  {:huga, [context: Makuro], Makuro} の形をパースする
  """
  def func_parse({func_atom, metadata, argument}, state) when (is_atom func_atom) and (is_atom argument) do
    state <> "{" <> to_string(func_atom) <> "}" <>  metadata_parse(metadata) <> "(" <> to_string(argument) <> ")"
  end


  @doc"""
  """
  def func_parse({func_atom, metadata, argument}, state) when (is_atom func_atom) and (is_atom argument) do
    state <> "{" <>  (to_string(func_atom)) <> "}"<> metadata_parse(metadata) <> "(" <> to_string(argument) <> ")"
  end

  @doc"""
    {{func_atom, metadata, arguments} , [context: Elixir, import: Kernel], argument} の形をパースする
  """
  def func_parse({func_info, metadata, argument} , state) when (is_list argument and is_tuple func_info) do
    argument_string = argument |> Enum.map(fn x -> func_parse(x, "") end) |> Enum.join(",")
    state <> "{" <> func_parse(func_info, "") <> "}"<> metadata_parse(metadata) <> "(" <> argument_string <> ")"
  end


  @doc"""
  {:hoge, [context: Elixir, import: Kernel], argument} の形をパースする
  """
  def func_parse({func_atom, metadata, argument} , state) when (is_list argument and is_atom func_atom) do
    argument_string = argument |> Enum.map(fn x -> func_parse(x, "") end) |> Enum.join(",")
    state <> "{" <> to_string(func_atom) <> "}" <> metadata_parse(metadata) <> "(" <> argument_string <> ")"
  end

  @doc"""
  atomのみ( [:IO] ) といったものを処理
  """
  def func_parse(func_atom, state) when is_atom func_atom do
    state <> to_string func_atom
  end

  @doc"""
  文字列がきた場合
  """
  def func_parse(str, state) when is_binary str do
    state <> str
  end

  @doc"""
  空リストの場合
  """
  def func_parse([], state) do
    state <> "[]"
  end

  @doc"""
  何かリストがきた場合
  """
  def func_parse(list, state) when is_list list do
    list|>
    Enum.map(fn x -> func_parse(x, state) end)
    |> Enum.join(",")
  end

  @doc"""
  何かタプルがきた場合 ex)無名関数の引数など
  """
  def func_parse(turple, state) when is_tuple turple do
    next_string = turple
    |> Tuple.to_list
    |> Enum.map(fn x -> func_parse(x, state)  end)
    |> Enum.join(",")
    state <> next_string
  end

  @doc"""
  metadata をパースするだけ
  """
  def metadata_parse([]) do "[]" end
  def metadata_parse(metadata) do
    meta_string = metadata
    |> Enum.map(fn {key, value} -> to_string(key) <> ":" <> to_string(value) end)
    |> Enum.join(",")

    "[" <> meta_string <> "]"
  end

end


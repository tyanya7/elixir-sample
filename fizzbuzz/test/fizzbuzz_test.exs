defmodule FizzbuzzTest do
  use ExUnit.Case
  doctest Fizzbuzz

  test "only other numbers" do
    assert Fizzbuzz.fizzbuzz(1, 2) == [1,2]
  end

  test "only fizz" do
    assert Fizzbuzz.fizzbuzz(3, 3) == ["Fizz"]
    assert Fizzbuzz.fizzbuzz(6, 6) == ["Fizz"]
  end

  test "only buzz" do
    assert Fizzbuzz.fizzbuzz(5, 5) == ["Buzz"]
    assert Fizzbuzz.fizzbuzz(10, 10) == ["Buzz"]
  end

  test "only fizzbuzz" do
    assert Fizzbuzz.fizzbuzz(15, 15) == ["FizzBuzz"]
    assert Fizzbuzz.fizzbuzz(30, 30) == ["FizzBuzz"]
  end
end

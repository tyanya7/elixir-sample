defmodule Fizzbuzz do
  def fizzbuzz(from, to) do
    from..to |> Enum.map(&(fizzbuzz/1))
  end

  defp fizzbuzz(num) when rem(num, 15) == 0 do
    "FizzBuzz"
  end

  defp fizzbuzz(num) when rem(num, 3) == 0 do
    "Fizz"
  end

  defp fizzbuzz(num) when rem(num, 5) == 0 do
    "Buzz"
  end

  defp fizzbuzz(num), do: num
end

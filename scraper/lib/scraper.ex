defmodule Scraper do
  use Hounde.Helpers

  def start do
    IO.puts "starting"
    Hound.start_session
    navigate_to "https://example.com/login"
    find_element(:id, "user_login") |> fill_field("rlord")
    el = find_element(:id, "user_pass")
    submit_element(el)
  end
end

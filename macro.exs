defmodule Macro_test do

  defmacro my_macro do
    quote do
      a = 1
    end
  end

  a = 2
  my_macro()
  IO.puts a
end


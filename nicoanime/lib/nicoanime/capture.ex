defmodule NicoAnime do
  def process() do
    nicospe_url
    |> HTTPoison.get()
    |> handle_response
    |> parse
  end

  def nicospe_url() do
    "http://ch.nicovideo.jp/anime-sp"
  end

  def handle_response({:ok, response}) do
    response.body
  end

  def parse(text) do
    Floki.find(text, ".g-live-contents")
    |> Floki.find(".g-live-title")
    |> Floki.find("a")
    |> Enum.map(fn {"a", _, [text]} ->
      String.trim_leading(text)
      |> String.trim_trailing end)
  end


end

defmodule NicoAnime.CLI do
  @default_count 10

  def main(args) do
    args
    |> parse_args
  end

  @doc """
  Parse argument
  if given :help or :h, return :help

  ## Example
      iex> NicoAnime.CLI.parse_args([:help])
      :help
  """
  def parse_args(argv) do
    parse = OptionParser.parse(argv, switches: [help: :boolean],
                                     aliases:  [h:     :help] )
    case parse do
      { [ help: true ], _, _ } -> :help
      { _, [user, project, count], _ } -> { user, project, String.to_integer(count)}
      { _, [user, project], _ } -> {user, project, @default_count}
      _ -> :help
    end
  end

end

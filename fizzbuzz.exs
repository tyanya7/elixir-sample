fizzbuzz = fn(number) ->
  cond do
    number > 100 ->
      IO.puts "error"
    rem(number, 15) == 0 ->
      IO.puts "fizzbuzz"
    rem(number, 3) == 0 ->
      IO.puts "fizz"
    rem(number, 5) == 0 ->
      IO.puts "buzz"
    true ->
      IO.puts Integer.to_string(number)
  end
end

for x <- 1..100 do
  fizzbuzz.(x)
end

# Github

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `githubapi` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:githubapi, "~> 0.1.0"}]
    end
    ```

  2. Ensure `githubapi` is started before your application:

    ```elixir
    def application do
      [applications: [:githubapi]]
    end
    ```


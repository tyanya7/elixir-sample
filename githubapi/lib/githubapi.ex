defmodule Github do
  require Logger

  def zen do
    url = "https://api.github.com/zen"
    response = HTTPoison.get!(url)
    Logger.info "response: #{inspect response}"
    body = response.body
    Logger.info "body: #{inspect body}"
    status_code = response.status_code
    Logger.info "status_code: #{inspect status_code}"
    headers = response.headers
    Enum.map(headers, fn{header, value} ->
      Logger.info "#{inspect header}: #{inspect value}" end)
  end

  def list_repos do
    url = "https://api.github.com/user/repos"
    token = "5d56ff31e8a4dcc3914ba7aac76e29c07249f1ce"
    headers = [{"Authorization", "token #{token}"}]
    response = HTTPoison.get!(url, headers)
    Logger.info "response: #{inspect response.body}"
  end
end

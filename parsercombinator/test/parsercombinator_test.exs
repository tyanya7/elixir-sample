defmodule ParsercombinatorTest do
  use ExUnit.Case
  doctest Parsercombinator

  test "seq parser test" do
    import Parsercombinator.Parser, only: [seq: 1, choice: 1, token: 1]
    parse = seq([token("foo"), choice([token("bar"), token("baz")])]);

    assert parse.("foobar", 0) == {true, ["foo", "bar"], 6}

    assert parse.("foobaz", 0) == {true, ["foo", "baz"], 6}

    assert parse.("foz", 0) == {false, nil, 0}
  end

  test "token function test" do
    import Parsercombinator.Parser, only: [token: 1]
    parse = token("foobar")

    assert parse.("foobar", 0) == {true, "foobar", 6}

    assert parse.("fuubar", 0) == {false, nil, 0}

    assert parse.("foobar", 1) == {false, nil, 1}
  end

  test "choice function test" do
    import Parsercombinator.Parser, only: [token: 1, choice: 1]
    parse = choice([token("hoge"), token("huga")])

    assert parse.("hogehuga", 0) == {true, "hoge", 4}

    assert parse.("hogehoge", 0) == {true, "hoge", 4}

    assert parse.("hugehuga", 0) == {false, nil, 0}
  end

  test "many function test" do
    import Parsercombinator.Parser, only: [token: 1, many: 1]
    parse = many(token("hoge"))

    assert parse.("hogehoge", 0) == {true, ["hoge", "hoge"], 8}

    assert parse.("hogehuga", 0) == {true, ["hoge"], 4}

    assert parse.("", 0) == {true, [], 0}
  end

  test "option function test" do
    import Parsercombinator.Parser, only: [token: 1, option: 1]
    parse = option(token("hoge"))

    assert parse.("hoge", 0) == {true, "hoge", 4}
    assert parse.("huga", 0) == {true, nil, 0}
  end

  test "regex function test" do
    import Parsercombinator.Parser, only: [regex: 1]
    parse = regex("hoge")

    assert parse.("hoge", 0) == {true, "hoge", 4}

    parse = regex("([1-9][0-9]*)")

    assert parse.("2014", 0) == {true, "2014", 4}

    assert parse.("01", 0) == {false, nil, 0}

  end

end

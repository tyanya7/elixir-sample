defmodule Parsercombinator.Parser do

  def token(str) do
    len = String.length(str)
    fn(target, position) ->
      result = if String.slice(target, position..(position+len-1))==str, do: true,  else: false
      if result, do: {true, str, position+len}, else: {false, nil, position}
    end
  end

  def many(parser) do
    fn(target, position) ->
      many_parser(parser, target, position)
    end
  end

  defp many_parser(parser, target, position, results \\ []) do
    case parser.(target, position) do
      {true, result, next_position} ->
        many_parser(parser, target, next_position, [result | results])
      {false, _, _} ->
        {true, results, position}
      nil ->
        {true, results, position}
    end
  end

  def choice(parsers) do
    fn(target, position) ->
      choice_parser(parsers, target, position)
    end
  end

  defp choice_parser(parsers, target, position) do
    l = parsers
    |> Enum.map(fn parser ->
      parser.(target, position) end)
    |> Enum.drop_while(fn {flag, _, _} -> !flag end)
    |> List.first
    if(l, do: l, else: {false, nil, position})
  end

  def seq(parsers) do
    fn(target, position) ->
      seq_parser(parsers, target, position)
    end
  end

  defp seq_parser(parsers, target, position, results \\ []) do
    result = parsers |> Enum.map(fn parser -> parser.(target, position) end)
    if Enum.any?(result, fn x -> match?({:true, _, _}, x) end) do
      {true, r, next_position} = Enum.filter(result, fn x -> match?({:true, _, _}, x) end) |> List.first
      seq_parser(parsers, target, next_position, [r | results])
    else
      if(Enum.empty?(results),do: {false, nil, position}, else: {true, Enum.reverse(results), position})
    end
  end

  def option(parser) do
    fn(target, position) ->
      option_parser(parser, target, position)
    end
  end

  defp option_parser(parser, target, position) do
    case  parser.(target, position) do
      {false, result, position} ->
        {true, result, position}
      x ->
        x
    end
  end

  def regex(str) do
    fn(target, position) ->
      r = Regex.compile!("^" <> str)
      len = String.length target
      result = Regex.run( r, String.slice(target, position..(position+len)), capture: :first )
      if result do
        {true, List.first(result), position+len}
      else
        {false, nil, position}
      end
    end
  end


end

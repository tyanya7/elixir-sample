defmodule Libincludetest do
  alias Libincludetest.AliasMe
  use Libincludetest.UseMe, "nyan", "huga"

  def alias_test do
    AliasMe.function
  end
end

defmodule Libincludetest.UseMe do
  defmacro __using__(value) do
    IO.puts(value)
    quote do
      def use_test do
        IO.puts "use test"
      end
    end
  end
end
